/* 
	Problem 3: Write a function that will return all
    cards that belong to a particular list based on the
    listID that is passed to it from the given data in cards.json.
    Then pass control back to the code that called it by using a callback function.
*/

const fs = require("fs");

const cardsPath = "data/cards.json";

const callback3 = (listId, cb) => {
  fs.readFile(cardsPath, "utf-8", (err, data) => {
    setTimeout(() => {
      if (err) {
        console.log("ERROR :- Can't read file");
      } else {
        const allCards = JSON.parse(data);
        const cardArray = allCards[listId];
        cb(cardArray);
      }
    }, 2 * 1000);
  });
};

module.exports = callback3;
