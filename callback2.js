/* 
	Problem 2: Write a function that will return all lists 
    that belong to a board based on the boardID that is passed 
    to it from the given data in lists.json. Then pass control 
    back to the code that called it by using a callback function.
*/
const fs = require("fs");

const listPath = "data/lists.json";

const callback2 = (boardId, cb) => {
  fs.readFile(listPath, "utf-8", (err, data) => {
    setTimeout(() => {
      if (err) {
        console.log("ERROR :- Can't read file");
      } else {
        const allLists = JSON.parse(data);
        const listArray = allLists[boardId];
        cb(listArray);
      }
    }, 2 * 1000);
  });
};

module.exports = callback2;
