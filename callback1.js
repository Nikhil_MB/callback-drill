// Problem 1: Write a function that will return a particular board's
// information based on the boardID that is passed from the given list
// of boards in boards.json and then pass control back to the code that called
// it by using a callback function.

const fs = require("fs");

const boardsPath = "data/boards.json";

const callback1 = (boardId, cb) => {
  fs.readFile(boardsPath, "utf-8", (err, data) => {
    setTimeout(() => {
      if (err) {
        console.log("ERROR :- Can't read file");
      } else {
        const boardInfo = JSON.parse(data).filter(
          (data) => data.id === boardId
        );
        cb(boardInfo[0]);
      }
    }, 2 * 1000);
  });
};

module.exports = callback1;
