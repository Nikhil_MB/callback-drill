/* 
	Problem 6: Write a function that will use the previously written functions 
    to get the following information. You do not need to pass control back to the 
    code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const callback1 = require("./callback1");
const callback2 = require("./callback2");
const callback3 = require("./callback3");

const callback6 = (boardId) => {
  callback1(boardId, (board) => {
    console.log("Board :-", board);

    callback2(board.id, (listArray) => {
      console.log("Lists :-", listArray);

      listArray.forEach((element) => {
        callback3(element.id, (cardArray) => {
          cardArray
            ? console.log(`${element.name} :-`, cardArray)
            : console.log(`${element.name} :- no cards found`);
        });
      });
    });
  });
};

module.exports = callback6;
