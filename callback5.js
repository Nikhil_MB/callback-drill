/* 
	Problem 5: Write a function that will use the previously written functions 
    to get the following information. You do not need to pass control back to the code 
    that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const callback1 = require("./callback1");
const callback2 = require("./callback2");
const callback3 = require("./callback3");

const callback5 = (boardId, cardName) => {
  callback1(boardId, (board) => {
    console.log("Board :-", board);

    callback2(board.id, (listArray) => {
      console.log("Lists :-", listArray);

      cardName.forEach((card) => {
        listArray.forEach((element) => {
          if (element.name === card) {
            callback3(element.id, (cardArray) => {
              console.log(`${card} :-`, cardArray);
            });
          }
        });
      });
    });
  });
};

module.exports = callback5;
