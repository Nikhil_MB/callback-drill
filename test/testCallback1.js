const callback1 = require("../callback1");

const boardId = "mcu453ed";

callback1(boardId, (boardInfo) => {
  console.log(boardInfo);
});
